<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Services;

use App\Models\CityModel;

/**
 * 城市管理-服务类
 * @author 牧羊人
 * @since 2020/8/30
 * Class CityService
 * @package App\Services
 */
class CityService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/8/30
     * CityService constructor.
     */
    public function __construct()
    {
        $this->model = new CityModel();
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2020/9/8
     * @author 牧羊人
     */
    public function getList()
    {
        // 请求参数
        $param = request()->all();
        // 上级ID
        $pid = getter($param, "pid", 0);
        // 查询条件
        $map = [
            ['pid', '=', $pid],
        ];
        // 部门名称
        $name = getter($param, "name");
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }
        $list = $this->model->getList($map, [["sort", "asc"]]);
        if (!empty($list)) {
            foreach ($list as &$val) {
                if ($val['level'] < 3) {
                    $val['haveChild'] = true;
                } else {
                    $val['haveChild'] = false;
                }
            }
        }
        return message("操作成功", true, $list);
    }
}
