<div id="{{$selectName}}" class="xm-select-demo"></div>
<script>
    layui.use(['xmSelect'], function () {
        var xmSelect = layui.xmSelect;
        var demo1 = xmSelect.render({
            el: '#{{$selectName}}',
            name: '{{$selectName}}',
            layVerify: '{{$selectRequired}}',
            tips: '请选择{{$selectTitle}}',
            empty: '呀, 没有数据呢',
            toolbar: {show: true},
            filterable: true,
            max: 100,
            maxMethod(seles, item) {
                alert(`不能选了, 已经超了`)
            },
            data: [
            @foreach ($selectData as $key => $val)
                @if (is_array($val))
                {name: '{{$val[$showName]}}', value: {{$val[$showValue]}}, @if (in_array($val[$showValue],$selectValue)) selected: true @endif},
                @else
                {name: '{{$val}}', value: {{$key}}, @if (in_array($key,$selectValue)) selected: true @endif},
                @endif
            @endforeach
            ]
        })
    });
</script>
